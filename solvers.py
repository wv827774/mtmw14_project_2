import numpy as np
import matplotlib.pyplot as plt
import math
from dictionaries import *

# Analytical solvers
def epsilon_func(gamma, L, beta):
    """Computes epsilon value for calculation of analytical solution for gyre simulation"""

    return gamma / (L * beta)


def b_func_analytic(epsilon):
    """Computes b coefficient for calculation of analytic solution for the gyre simulation"""

    return (-1 + np.sqrt(1 + (2 * np.pi * epsilon) ** 2)) / (2 * epsilon)


def a_func_analytic(epsilon):
    """Computes a coefficient for calculation of analytic solution for the gyre simulation"""

    return (-1 - np.sqrt(1 + (2 * np.pi * epsilon)**2)) / (2 * epsilon)


def f1_func_analytic(x, a, b):
    """f1 function for calculation of analytical solution for gyre"""
    # Defining e beforehand for improved readability
    exp = math.exp
    # Computing numerator and denominator separately for improved readability
    numerator = (exp(a) - 1)*exp(b*x) + (1 - exp(b))*exp(a*x)
    denominator = exp(b) - exp(a)

    return np.pi*(1 + numerator/denominator)


def f2_func_analytic(x, a, b):
    """f2 function for calculation of analytical solution for gyre"""

    # Defining e beforehand for improved readability
    exp = math.exp
    # Computing numerator and denominator separately for improved readability
    numerator = (exp(a) - 1)*b*exp(b*x) + (1 - exp(b))*a*exp(a*x)
    denominator = exp(b) - exp(a)
    return numerator/denominator


def Mushgrave_analytic(setup):
    """ Main analytic solver.
    Computes the analytic solution to the shallow water equations, using the approach of Mushgrave (1985)"""

    # Extracting number of grid points in x and y direction and grid spacing
    x_points = setup['x_points']  # Note this is a number, not an array
    y_points = setup['y_points']
    grid_spacing = setup['grid_spacing']

    # Defining arrays for x and y as distance from origin
    x = np.arange(x_points + 1)*grid_spacing
    y = np.arange(y_points + 1)*grid_spacing

    # Extracting constants from setup (see analytic_dictionaries)
    tau_0 = setup['tau_0']
    H = setup['H']
    L = setup['L']
    gamma = setup['gamma']
    rho = setup['rho']
    f_0 = setup['f_0']
    beta = setup['beta']
    g = setup['g']
    eta_0 = setup['eta_0']

    # Defining arrays for u,v and eta (note that y-axis is the first index)
    u_analytic = np.zeros((y_points, x_points))
    v_analytic = np.zeros((y_points, x_points))
    eta_analytic = np.zeros((y_points, x_points))

    # Computing epsilon for values of a and b
    epsilon = epsilon_func(gamma, L, beta)

    # Computing a and b for use in functions f1 and f2
    a = a_func_analytic(epsilon)
    b = b_func_analytic(epsilon)

    # defining common coefficient term for improved readability
    coefficient = tau_0 / (np.pi * gamma * rho * H)

    # Computing u, v and eta over domain
    for j in range(y_points):
        for i in range(x_points):
            # Collating common terms for improved readability
            cos_term = np.cos(np.pi*y[j]/L)
            sin_term = np.sin(np.pi*y[j]/L)

            # computing u, v and eta as function of x and y
            u_analytic[j, i] = -coefficient * f1_func_analytic(x[i]/L, a, b) * cos_term  # zonal velocity term
            v_analytic[j, i] = coefficient * f2_func_analytic(x[i]/L, a, b) * sin_term  # meridional velocity term
            eta_analytic[j, i] = eta_0 + coefficient * f_0 * L / g * (
                        gamma / (f_0 * np.pi) * f2_func_analytic(x[i]/L, a, b) * cos_term
                        + 1 / np.pi * f1_func_analytic(x[i]/L, a, b) * (
                                    sin_term * (1 + beta * y[j] / f_0) +
                                    (beta * L) / (f_0 * np.pi) * cos_term))  # displacement term

    # Returning values of u, v, and eta for energy computations
    return u_analytic, v_analytic, eta_analytic, x, y


def taskC_plots(setup):
    """ Uses inputs of u, v and eta from analytic solver to plot contour plots of all three variables for task c"""
    # Extracting u, v, and eta, and plotting arrays from solver
    u, v, eta, x, y = Mushgrave_analytic(setup)

    # Plotting eta

    fig, ax = plt.subplots()
    c1 = ax.pcolormesh(x/1000, y/1000, eta, cmap='PRGn')
    ax.set_xlabel('x (km)')
    ax.set_ylabel('y (km)')
    cbar1 = fig.colorbar(c1)
    cbar1.set_label('metres')
    plt.title('eta analytical')
    plt.savefig(setup['fig_name_eta'])
    # Plotting v
    fig, ax2 = plt.subplots()
    c2 = ax2.pcolormesh(x/1000, y/1000, v, cmap='bwr')
    ax2.set_xlabel('x (km)')
    ax2.set_ylabel('y (km)')
    cbar2 = fig.colorbar(c2)
    cbar2.set_label('ms-1')
    plt.title('v analytical')
    plt.savefig(setup['fig_name_v'])
    # Plotting u
    fig, ax3 = plt.subplots()
    c3 = ax3.pcolormesh(x/1000, y/1000, u, cmap='bwr')
    ax3.set_xlabel('x (km)')
    ax3.set_ylabel('y (km)')
    cbar3 = fig.colorbar(c3)
    cbar3.set_label('ms-1')
    plt.savefig(setup['fig_name_u'])
    plt.title('u analytical')
    plt.show()



# Numerical solvers

def wind_stress_zon(y_array_ugrid, x_points, L, tau_0):
    """Computes zonal wind stress using meridional distance from origin (y_points), length of the domain, L,
    and constant, tau_0. Then maps zonal wind stress onto an Arakawa C grid with total points in u, x_points """

    # computing zonal wind stress as function of position in y
    tau_zonal = tau_0*-np.cos(np.pi*y_array_ugrid/L)
    # tiling and reshaping array so that values f wind stress are mapped onto the Arakawa (u) c-grid
    return np.tile(tau_zonal, (x_points + 1, 1)).transpose()


def compute_coriolis_ugrid(y_array_ugrid, x_points, f_0, beta):
    """ Computes coriolis (using f plane approximation - f = f_0 + beta*y) and then maps this onto u for an
    Arakawa C grid, where x_points and  points are the number of points in x for eta and y_array_ugrid gives
    meridional distance from the origin at each point in y for a u grid"""

    # Calculating coriolis as a function of y
    coriolis = f_0 + (beta*y_array_ugrid)

    # reshaping into second axis (rather than first) for mapping onto Ararkawa C-grid (u grid)
    return np.tile(coriolis, (x_points + 1, 1)).transpose()


def compute_coriolis_vgrid(y_array_vgrid, x_points, f_0, beta):
    """ Computes coriolis parameter (using f plane approximation - f = f_0 + beta*y) and then maps this onto an
     Arakawa C grid (v grid), where x_points indicates the number of points in x for eta and y_array_vgrid gives
     meridional distance from the origin at each point in y for a v grid"""

    # Calculating coriolis as a function of y
    coriolis = f_0 + (beta*y_array_vgrid)

    return np.tile(coriolis, (x_points, 1)).transpose()


def v_interp_onto_ugrid(v, y_points):
    """ Interpolates values for meridional velocity v onto a corresponding index in
    zonal velocity, u, on an Arakawa C grid"""
    # creating array used for appending zeros to start/end of v arrays. Same
    # dimensions on first axis (y-axis) as u grid (and v grid after slicing).
    zeros_array_x = np.zeros((y_points, 1))

    # slicing for forwards differencing in y (axis 0) and appending zeros for backwards differencing in x (second axis)
    v_same_index = np.concatenate((v[:-1, :], zeros_array_x), axis=1)
    v_j_iminus1 = np.concatenate((zeros_array_x, v[:-1, :]), axis=1)
    v_jplus1_i = np.concatenate((v[1:, :], zeros_array_x), axis=1)
    v_jplus1_iminus1 = np.concatenate((zeros_array_x, v[1:, :]), axis=1)

    return (v_same_index + v_j_iminus1 + v_jplus1_i + v_jplus1_iminus1)/4


def u_interp_onto_vgrid(u, x_points):
    """Interpolates values for zonal velocity v onto a corresponding index in
    meridional velocity, v, on an Arakawa C grid"""
    zeros_array_y = np.zeros((1, x_points))

    # slicing for forwards differencing in x (second axis) #
    # and appending zeros for backwards differencing in y (first axis)
    u_same_index = np.concatenate((u[:, :-1], zeros_array_y), axis=0)
    u_j_iplus1 = np.concatenate((u[:, 1:], zeros_array_y), axis=0)
    u_jminus1_i = np.concatenate((zeros_array_y, u[:, :-1]), axis=0)
    u_jminus1_iplus1 = np.concatenate((zeros_array_y, u[:, 1:]), axis=0)

    return (u_same_index + u_j_iplus1 + u_jminus1_i + u_jminus1_iplus1)/4


def eta_x_gradient(eta, y_points, dx):
    """computes gradient of eta w.r.t x on an Arakawa C grid (u grid), using grid spacing dx and number of eta
    grid points in the meridional, y_points"""
    # creating array used for appending zeros to start/end of eta arrays. Same dimensions on
    # first axis (y-axis) as eta grid.
    zeros_array_x = np.zeros((y_points, 1))

    eta_same_index = np.concatenate((eta, zeros_array_x), axis=1)
    eta_j_iminus1 = np.concatenate((zeros_array_x, eta), axis=1)

    return (eta_same_index - eta_j_iminus1)/dx  # computes gradient by dividing by grid spacing


def eta_y_gradient(eta, x_points, dy):
    """computes gradient of eta w.r.t y on an Arakawa C grid (v grid), using grid spacing dy and number of eta
    grid points in the meridional, y_points"""
    # creating array used for appending zeros to start/end of eta arrays. Same dimensions on
    # second axis (x-axis) as eta grid.
    zeros_array_y = np.zeros((1, x_points))

    eta_same_index = np.concatenate((eta, zeros_array_y), axis=0)
    eta_jminus1 = np.concatenate((zeros_array_y, eta), axis=0)

    return (eta_same_index - eta_jminus1)/dy


def compute_energy(eta, u, v, dx, dy, rho, H, g):
    """Computes total energy over a 2_d domain using diagnostics of eta (free surface height), u and v (zonal and
    meridional velocities respectively). Also using domain depth H, water density rho and gravity g, along with dx
    dy which represent the grid spacings in x and y respectively"""

    # computing energy at each grid cell, slicing elements in u and v arrays where no corresponding index exists for eta
    energy_per_grid = 0.5*rho*(H*(u**2 + v**2) + g*eta**2)

    # Computing integral of energy over domain, by summing all points and multiplying by grid spacing in y and x
    return np.sum(energy_per_grid)*dx*dy


def gyre_numerical(setup):

    """Main Numerical solver. Computes a numerical solution for the Stommel western boundary layer solution"""

    # Extracting constants from dictionary
    f_0 = setup['f_0']
    beta = setup['beta']
    g = setup['g']
    gamma = setup['gamma']
    rho = setup['rho']
    H = setup['H']
    L = setup['L']
    tau_0 = setup['tau_0']

    # Extracting model grid/runtime parameters
    x_points = setup['x_points']  # number of points in x
    y_points = setup['y_points']  # number of points in y
    dx = setup['dx']  # spacing of grid points in x
    dy = setup['dy']  # spacing of grid points in y
    dt = setup['dt']  # time step interval
    nt = setup['nt']  # number of time steps

    # Extracting flag to determine whether higher resolution run is being performed, and for plotting
    high_res_flag = setup['high_res_flag']
    task_flag = setup['task_flag']

    # Defining scalar arrays for y and x for plotting (distance from origin in metres)
    x_array_plotting = np.arange(x_points + 1)*dx
    y_array_plotting = np.arange(y_points + 1)*dy
    x_array_eta_plotting = np.arange(x_points)*dx

    # Defining y-array with dimensions of u grid and v grid, giving distance from origin (metres)
    y_array_ugrid = np.arange(y_points)*dy
    y_array_vgrid = np.arange(y_points + 1)*dy

    # Defining 2-D arrays for surface displacement, eta, and meridional and zonal velocities, v and u respectively
    # Note: y-axis indexing is first and extra point in meridional for v and zonal for u.
    eta = np.zeros((y_points, x_points))
    v = np.zeros((y_points + 1, x_points))
    u = np.zeros((y_points, x_points + 1))

    # Defining empty arrays for total system energy, energy difference (between numerical and analytical)
    # and associated time series (for appending and plotting)
    total_energy = np.empty(0)
    energy_difference = np.empty(0)
    time = np.empty(0)

    # Defining zonal and meridional wind stresses
    tau_zonal = wind_stress_zon(y_array_ugrid, x_points, L, tau_0)
    tau_meridional = setup['tau_meridional']

    # Computing values of coriolis parameter based upon f_0, beta, and location in meridional,
    # for both u grid and v grid
    coriolis_ugrid = compute_coriolis_ugrid(y_array_ugrid, x_points, f_0, beta)
    coriolis_vgrid = compute_coriolis_vgrid(y_array_vgrid, x_points, f_0, beta)

    # Extracting analytical values for comparison to numerical values to determine model efficiency. Either standard
    # or high resolution run is chosen
    if high_res_flag == 'on':
        u_analytic, v_analytic, eta_analytic, x, y = Mushgrave_analytic(setup_analytic_high_res)
    else:
        u_analytic, v_analytic, eta_analytic, x, y = Mushgrave_analytic(setup_analytic)

    # Looping over time with nt-2 iterations and an interval of 2, to account for use of a forwards-backwards
    # scheme (jumps forwards two time steps)
    for n in range(0, nt - 2, 2):

        # Interpolating v and u onto eta grid to compute energy of solution
        u_on_eta = (u[:, 1:] + u[:, :-1]) / 2
        v_on_eta = (v[1:, :] + v[:-1, :]) / 2

        # Computing the offset between the analytical and steady state solutions for energy calculations
        eta_prime = eta_analytic - eta
        u_prime = u_analytic - u_on_eta
        v_prime = v_analytic - v_on_eta

        # Computing total energy in the system at the current time step,  along with energy difference between
        # numerical and analytical solution
        total_energy = np.append(total_energy,[compute_energy(eta, u_on_eta, v_on_eta, dx, dy, rho, H, g)])
        energy_difference = np.append(energy_difference, [compute_energy(eta_prime, u_prime, v_prime, dx, dy, rho, H, g)])
        time = np.append(time, [n*dt])

        # Computing eta_new - the value of eta at the next time step. Slicing u and v to mimic a forwards difference
        eta_new = eta - H*dt*((u[:, 1:] - u[:, :-1])/dx + (v[1:, :] - v[:-1, :])/dy)
        # Computing u_new - the value of u at the next time step, interpolating for coriolis
        u_new = u + coriolis_ugrid*dt*v_interp_onto_ugrid(v, y_points) - g*dt*eta_x_gradient(eta_new, y_points, dx) \
                - gamma*dt*u + tau_zonal*dt/(rho*H)
        # Ensuring no slip conditions at boundary
        u_new[:, 0] = 0
        u_new[:, x_points] = 0

        # Computing v_new - the value of v at the next time step
        v_new = v - coriolis_vgrid*dt*u_interp_onto_vgrid(u_new, x_points) - g*dt*eta_y_gradient(eta_new, x_points, dy)\
                - gamma*dt*v + tau_meridional*dt/(rho*H)
        # Ensuring no slip conditions at boundary
        v_new[0, :] = 0
        v_new[y_points, :] = 0

        # computing eta_new2 - the value of eta at the time step after the next time step
        eta_new2 = eta_new - H*dt*((u_new[:, 1:] - u_new[:, :-1])/dx + (v_new[1:, :] - v_new[:-1, :])/dy)

        # computing v_new2 - the value of v at the time step after the next time step
        v_new2 = v_new - coriolis_vgrid*dt*u_interp_onto_vgrid(u_new, x_points) - g*dt*eta_y_gradient(eta_new2, x_points, dy) \
                 - gamma*dt*v_new + tau_meridional*dt/(rho*H)
        # Ensuring no-slip conditions at the time step after
        v_new2[0, :] = 0
        v_new2[y_points, :] = 0

        # computing u_new2 - the value of u at the time step after the next time step
        u_new2 = u_new + coriolis_ugrid*dt*v_interp_onto_ugrid(v_new2, y_points) - g*dt*eta_x_gradient(eta_new2, y_points, dx) \
                 - gamma*dt*u_new + tau_zonal*dt/(rho*H)
        # Ensuring no sip conditions at boundaries
        u_new2[:, 0] = 0
        u_new2[:, x_points] = 0

        # Updating arrays at the end of the time step to the value at n+2 time step
        eta = eta_new2.copy()
        u = u_new2.copy()
        v = v_new2.copy()

    # Printing output for eta_0 to use in the analytical
    print('eta_0 value is', eta[int(y_points/2), 0])

    # Printing the offset between the analytical and steady state solutions for energy calculations, or printing the
    # final steady state energy
    if task_flag == 'task_d_difference' or task_flag == 'task_e2':
        eta_prime = eta_analytic - eta
        u_prime = u_analytic - u_on_eta
        v_prime = v_analytic - v_on_eta
        final_energy_difference = energy_difference[-1]
        print('Final energy difference is', final_energy_difference, 'J')
    elif task_flag == 'task_e':
        print('Steady state energy value is', total_energy[-1],'J')

    # Plotting outputs - makes plots depending upon task

    if task_flag == 'task_d' or task_flag == 'task_dst' or task_flag == 't50_100_days' or task_flag == 't70_100_days':
        # Plotting eta
        fig, ax = plt.subplots()
        c1 = ax.pcolormesh(x_array_plotting / 1000, y_array_plotting / 1000, eta[:, :], cmap='PRGn')
        ax.set_xlabel('x (km)')
        ax.set_ylabel('y (km)')
        fig.colorbar(c1)
        plt.title('Surface displacement contour plot')
        plt.savefig(setup['fig_name_eta'])

    # Plotting energy time series for task e
    if task_flag == 'task_e':
        fig, ax4 = plt.subplots()
        ax4.plot((time/86400), total_energy, label='total energy (joules)')
        ax4.set_xlabel('Time (days)')
        ax4.set_ylabel('Energy (joules)')
        plt.savefig('energy_time_series')
        plt.title('Energy time series')

    # Plotting cross-sections for task D
    if task_flag == 'task_d' or task_flag == 'task_dst':
        # Plotting u against x closest to the western edge of the basin
        fig, ax5 = plt.subplots()
        ax5.plot(x_array_plotting/1000, u[0, :], color='orange')
        ax5.set_xlabel('x (km)')
        ax5.set_ylabel('Zonal velocity (ms-1)')
        plt.title('Zonal velocity against x at the southern edge')
        # Plotting v against y closest to the western edge of the basin
        fig, ax6 = plt.subplots()
        ax6.plot(y_array_plotting/1000, v[:, 1], color='cyan')
        ax6.set_xlabel('y (km)')
        ax6.set_ylabel('Meridional velocity (ms-1)')
        plt.title('Meridional velocity against y at the western edge')
        # Plotting eta against x through the middle of the gyre
        fig, ax7 = plt.subplots()
        ax7.plot(x_array_eta_plotting/1000, eta[int(y_points/2), :], color='black')
        ax7.set_xlabel('y (km)')
        ax7.set_ylabel('Surface displacement (m)')
        plt.title('Surface displacement against x through the centre of the gyre')

    # Plotting difference contour plots
    if task_flag == 'task_d_difference' or task_flag == 'task_e2':
        # Plotting eta prime contour plot
        fig, ax8 = plt.subplots()
        c8 = ax8.pcolormesh(x_array_plotting/1000, y_array_plotting/1000, eta_prime[:, :], cmap='PRGn')
        ax8.set_xlabel('x (km)')
        ax8.set_ylabel('y (km)')
        fig.colorbar(c8)
        plt.title('eta difference contour plot')
        plt.savefig(setup['fig_name_eta'])
        # Plotting v prime contour plot
        fig, ax9 = plt.subplots()
        c9 = ax9.pcolormesh(x_array_plotting/1000, y_array_plotting/1000, v_prime[:, :], cmap='bwr')
        ax9.set_xlabel('x (km)')
        ax9.set_ylabel('y (km)')
        fig.colorbar(c9)
        plt.title('v difference contour plot')
        plt.savefig(setup['fig_name_v'])
        # Plotting u_prime contour plot
        fig, ax10 = plt.subplots()
        c10 = ax10.pcolormesh(x_array_plotting/1000, y_array_plotting/1000, u_prime[:, :], cmap='bwr')
        ax10.set_xlabel('x (km)')
        ax10.set_ylabel('y (km)')
        fig.colorbar(c10)
        plt.title('u difference contour plot')
        plt.savefig(setup['fig_name_u'])

    # Plotting steady state velocity plots
    if task_flag == 'task_dst':
        # Plotting contour plot of v
        fig, ax2 = plt.subplots()
        c2 = ax2.pcolormesh(x_array_plotting/1000, y_array_plotting/1000, v[:, :], cmap='bwr')
        ax2.set_xlabel('x (km)')
        ax2.set_ylabel('y (km)')
        fig.colorbar(c2)
        plt.title('Meridional velocity contour plot')
        plt.savefig(setup['fig_name_v'])
        # Plotting contour plot of u
        fig, ax3 = plt.subplots()
        c3 = ax3.pcolormesh(x_array_plotting/1000, y_array_plotting/1000, u[:, :], cmap='bwr')
        ax3.set_xlabel('x (km)')
        ax3.set_ylabel('y (km)')
        fig.colorbar(c3)
        plt.title('Zonal velocity contour plot')
        plt.savefig(setup['fig_name_u'])
    plt.show()





