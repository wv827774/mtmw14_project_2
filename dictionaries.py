"""Dictionaries for all tasks and plots"""


# Analytic dictionaries

setup_analytic = {'tau_0': 0.2,  # Wind stress vector constant (Nm-2)
                  'f_0': 10**-4,  # Coriolis parameter (s-1)
                  'beta': 10**-11,  # Beta value (m-1s-1)
                  'g': 10,  # Gravitational constant (ms-2)
                  'gamma': 10**-6,  # Linear drag coefficient (s-1)
                  'rho': 1000,  # Seawater density (kgm-3)
                  'H': 1000,  # Resting depth of fluid, assumed constant (m)
                  'L': 10**6,  # Length of domain (m)
                  'eta_0': -0.1145,  # Unknown constant of integration (OBTAINED FROM STEADY STATE ANALYTICAL SOLUTION)
                  'grid_spacing': 10000,  # Spacing of grid cells in both x and y direction (metres)
                  'y_points': 100,  # Number of grid points in x
                  'x_points': 100,  # Number of grid points in y
                  'fig_name_eta': 'eta_analytic',  # name of saved figure for eta output
                  'fig_name_u': 'u_analytic',  # name of saved figure for u output
                  'fig_name_v': 'v_analytic'}  # name of saved figure for v output

setup_analytic_high_res = {'tau_0': 0.2,  # Wind stress vector constant (Nm-2)
                  'f_0': 10**-4,  # Coriolis parameter (s-1)
                  'beta': 10**-11,  # Beta value (m-1s-1)
                  'g': 10,  # Gravitational constant (ms-2)
                  'gamma': 10**-6,  # Linear drag coefficient (s-1)
                  'rho': 1000,  # Seawater density (kgm-3)
                  'H': 1000,  # Resting depth of fluid, assumed constant (m)
                  'L': 10**6,  # Length of domain (m)
                  'eta_0': -0.1145,  # Unknown constant of integration (OBTAINED FROM STEADY STATE ANALYTICAL SOLUTION)
                  'grid_spacing': 5000,  # Spacing of grid cells in both x and y direction (metres)
                  'y_points': 200,  # Number of grid points in x
                  'x_points': 200,  # Number of grid points in y
                  'fig_name_eta': 'eta_analytic',  # name of saved figure for eta output
                  'fig_name_u': 'u_analytic',  # name of saved figure for u output
                  'fig_name_v': 'v_analytic'}  # name of saved figure for v output

# Numerical dictionaries #

setup_numerical_D1day = {'tau_0': 0.2,  # Wind stress constant
                   'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                   'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                   'beta': 10 ** -11,  # Beta value (m-1s-1)
                   'g': 10,  # Gravitational constant (ms-2)
                   'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                   'rho': 1000,  # Seawater density (kgm-3)
                   'H': 1000,  # Resting depth of fluid, assumed constant (m)
                   'L': 10 ** 6,  # Length of domain (m)
                   'dt': 50,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                   'nt': int(86400/50),  # Number of timesteps (to ensure a 1 day model runtime)
                   'x_points': 100,  # number of grid pointls in x
                   'y_points': 100,  # number of grid points in y
                   'dx': 10 ** 4,  # grid spacing in x
                   'dy': 10 ** 4,  # grid spacing in y
                   'fig_name_eta': 'eta_t=1day',  # name of saved figure for eta output
                   'fig_name_u': 'u_t=1day',  # name of saved figure for u output
                   'fig_name_v': 'v_t=1day',  # name of saved figure for v output
                   'fig_name_u_cross_section:': 'u_cross_section_1day',
                   'fig_name_v_cross_section:': 'v_cross_section_1day',
                   'fig_name_eta_cross_section:': 'eta_cross_section_1day',
                   'high_res_flag': 'off',
                   'task_flag': 'task_d'}

setup_numerical_D_steady_state = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 50,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/50)*50,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 100,  # number of grid points in x
                           'y_points': 100,  # number of grid points in y
                           'dx': 10 ** 4,  # grid spacing in x
                           'dy': 10 ** 4,  # grid spacing in y
                           'fig_name_eta': 'eta_steadyState',  # name of saved figure for eta output
                           'fig_name_u': 'u_steadyState',  # name of saved figure for u output
                           'fig_name_v': 'v_steadyState',
                           'high_res_flag': 'off',
                           'task_flag': 'task_dst'}  # name of saved figure for v output

setup_numerical_D_difference = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 50,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/50)*50,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 100,  # number of grid points in x
                           'y_points': 100,  # number of grid points in y
                           'dx': 10 ** 4,  # grid spacing in x
                           'dy': 10 ** 4,  # grid spacing in y
                           'fig_name_eta': 'eta_steadyState',  # name of saved figure for eta output
                           'fig_name_u': 'u_steadyState',  # name of saved figure for u output
                           'fig_name_v': 'v_steadyState',
                           'high_res_flag': 'off',
                           'task_flag': 'task_d_difference'}  # name of saved figure for v output

setup_numerical_E1 = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 50,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/50)*50,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 100,  # number of grid points in x
                           'y_points': 100,  # number of grid points in y
                           'dx': 10 ** 4,  # grid spacing in x
                           'dy': 10 ** 4,  # grid spacing in y
                           'fig_name_eta': 'eta_t=50days',  # name of saved figure for eta output
                           'fig_name_u': 'u_t=50days',  # name of saved figure for u output
                           'fig_name_v': 'v_t=50days',
                           'high_res_flag': 'off',
                           'task_flag': 'task_e'}  # name of saved figure for v output


setup_numerical_100_days_t50 = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 50,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/50)*100,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 100,  # number of grid points in x
                           'y_points': 100,  # number of grid points in y
                           'dx': 10 ** 4,  # grid spacing in x
                           'dy': 10 ** 4,  # grid spacing in y
                           'fig_name_eta': 'eta_t=100days',  # name of saved figure for eta output
                           'fig_name_u': 'u_t=100days',  # name of saved figure for u output
                           'fig_name_v': 'v_t=100days', # name of saved figure for v output
                           'fig_name_energy': 'energy_steady_state',
                           'fig_name_u_cross_section:': 'u_cross_section_steady_state',
                           'fig_name_v_cross_section:': 'v_cross_section_steady_state',
                           'fig_name_eta_cross_section:': 'eta_cross_section_steady_state',
                            'high_res_flag': 'off',
                               'task_flag': 't50_100_days' }

setup_numerical_100_days_t70 = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 70,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/70)*100,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 100,  # number of grid points in x
                           'y_points': 100,  # number of grid points in y
                           'dx': 10 ** 4,  # grid spacing in x
                           'dy': 10 ** 4,  # grid spacing in y
                           'fig_name_eta': 'eta_dt=70',  # name of saved figure for eta output
                           'fig_name_u': 'u_dt=70',  # name of saved figure for u output
                           'fig_name_v': 'v_dt=70',
                               'high_res_flag': 'off',
                                'task_flag': 't70_100_days'}

setup_numerical_high_resolution_steady_state = {'tau_0': 0.2,  # Wind stress constant
                           'tau_meridional': 0,  # meridional wind stress (assumed zero for now)
                           'f_0': 10 ** -4,  # Coriolis parameter (s-1)
                           'beta': 10 ** -11,  # Beta value (m-1s-1)
                           'g': 10,  # Gravitational constant (ms-2)
                           'gamma': 10 ** -6,  # Linear drag coefficient (s-1)
                           'rho': 1000,  # Seawater density (kgm-3)
                           'H': 1000,  # Resting depth of fluid, assumed constant (m)
                           'L': 10 ** 6,  # Length of domain (m)
                           'dt': 25,  # time step (conservative based pon average flow speeds in WBC- Stommel)
                           'nt': int(86400/25)*40,  # Number of timesteps (to ensure a 1 day model runtime)
                           'x_points': 200,  # number of grid points in x
                           'y_points': 200,  # number of grid points in y
                           'dx': 5000,  # grid spacing in x (metres)
                           'dy': 5000,  # grid spacing in y (metres)
                           'fig_name_eta': 'eta_t_highRes_st',  # name of saved figure for eta output
                           'fig_name_u': 'u_t_highRes_st',  # name of saved figure for u output
                           'fig_name_v': 'v_t_highRes_st',   # name of saved figure for v output
                           'fig_name_energy': 'energy_highRes_st',
                           'fig_name_u_cross_section:': 'u_cross_section_highRes_st',
                           'fig_name_v_cross_section:': 'v_cross_section_highRes_st',
                           'fig_name_eta_cross_section:': 'eta_cross_section_highRes_st',
                                                'high_res_flag': 'on',
                                                'task_flag': 'task_e2'}
