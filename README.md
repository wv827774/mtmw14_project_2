'Report' is a self-contained notebook containing both analysis and the code to produce the graphs. 
Solvers contains solver functions for all tasks.
Dictionaries contains dictionaries (parameters) used in all tasks.

Note that in the report, some of the figures haven't loaded in. However, running the code will produce them. 
